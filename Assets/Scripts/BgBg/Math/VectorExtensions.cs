using UnityEngine;

namespace BgBg.Plugins.Math.VectorExtensions
{
    public static class VectorExtensions
    {
        public static Vector3 Vector3Absolute(Vector3 _Vector3)
        {
            _Vector3=new Vector3(Mathf.Abs(_Vector3.x),Mathf.Abs(_Vector3.y),Mathf.Abs(_Vector3.z));
            return _Vector3;
        }
    }
}
