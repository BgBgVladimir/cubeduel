using System.Collections;
using UnityEngine;
using BgBg.Plugins.Math.VectorExtensions;

public class RollingAnimations:MonoBehaviour
{
    [SerializeField]
    PlayerController player;

    Vector3 sourceScale;

    private void Awake()
    {
        player=GetComponent<PlayerController>();
        //remember sourceScale
        sourceScale=transform.localScale;
    }
    private void Start()
    {
        StartCoroutine(TakeAnimation());
    }

    IEnumerator TakeAnimation()
    {
        if(player.sitAnimation)
        {
            StartCoroutine(Sit());
        }
        else if(player.jumpAnimation)
        {
            StartCoroutine(Jump());
        }
        else if(player.leftAnimation)
        {
            StartCoroutine(MoweLeft());
        }
        else if(player.rightAnimation)
        {
            StartCoroutine(MoweRight());
        }
        else
        {
            StartCoroutine(MoweForward());
        }
        yield break;
    }

    IEnumerator HalfTurn(Vector3 moweDirection,Vector3 upDirection,Vector3 rotateDirection,float speedMultiplier = 1)
    {
        float localSpeed;
        //remember source params
        Vector3 sourcePosition = transform.position;
        Quaternion sourceRotation = transform.rotation;
        //do animation "half turn"
        while(Vector3.Distance(new Vector3(transform.position.x,0,transform.position.z),new Vector3(sourcePosition.x,0,sourcePosition.z))<=sourceScale.x/2f)
        {
            yield return new WaitForSeconds(Time.fixedDeltaTime);
            localSpeed=player.speed*speedMultiplier;
            transform.position+=(moweDirection*localSpeed+(upDirection*(localSpeed/2.5f)));
            transform.Rotate(rotateDirection*(localSpeed*90),Space.World);
        }
        //end frame animation half turn
        yield return new WaitForSeconds(Time.fixedDeltaTime);
        transform.position=(sourcePosition+(sourceScale.x/2f)*moweDirection)+upDirection*sourceScale.x/5f;
        transform.rotation=sourceRotation;
        transform.Rotate(rotateDirection*45f,Space.World);
        yield break;
    }
    IEnumerator MoweForward()
    {
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.up,Vector3.right));
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.down,Vector3.right));
        yield return new WaitForSeconds(Time.fixedDeltaTime);
        yield return new WaitForSeconds(Time.fixedDeltaTime);
        StartCoroutine(TakeAnimation());
        yield break;
    }
    IEnumerator MoweLeft()
    {
        yield return StartCoroutine(HalfTurn(Vector3.left,Vector3.up,Vector3.forward,2));//mowe 1/2 left x2 speed
        yield return StartCoroutine(HalfTurn(Vector3.left,Vector3.down,Vector3.forward,2));//mowe 1/2 left x2 speed
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.up,Vector3.right,2));//mowe 1/2 forward x2 speed
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.down,Vector3.right,2));//mowe 1/2 forward x2 speed
        StartCoroutine(TakeAnimation());
        yield break;
    }
    IEnumerator MoweRight()
    {
        yield return StartCoroutine(HalfTurn(Vector3.right,Vector3.up,-Vector3.forward,2));//mowe 1/2 right x2 speed
        yield return StartCoroutine(HalfTurn(Vector3.right,Vector3.down,-Vector3.forward,2));//mowe 1/2 right x2 speed
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.up,Vector3.right,2));//mowe 1/2 forward x2 speed
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.down,Vector3.right,2));//mowe 1/2 forward x2 speed
        StartCoroutine(TakeAnimation());
        yield break;
    }

    IEnumerator Sit()
    {
        Vector3 upAxis = VectorExtensions.Vector3Absolute(transform.InverseTransformDirection(Vector3.up+Vector3.forward));
        if(player.sitAnimation)
        {
            StartCoroutine(SitDown(upAxis));
        }
        while(player.sitAnimation)
        {
            yield return new WaitForSeconds(Time.fixedDeltaTime);
            yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.up,Vector3.right));
            yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.down,Vector3.right));
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
        StartCoroutine(GetUp(upAxis));
        StartCoroutine(TakeAnimation());
        yield break;
    }
    IEnumerator SitDown(Vector3 upAxis)
    {
        float sourcePositionY = transform.position.y;
        transform.position=new Vector3(transform.position.x,sourcePositionY-sourceScale.magnitude/8,transform.position.z);
        while((sourceScale-upAxis/2).magnitude<(transform.localScale).magnitude)
        {
            transform.localScale-=upAxis*player.sitSpeed;
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
        transform.localScale=sourceScale-upAxis/2;
        yield break;
    }
    IEnumerator GetUp(Vector3 upAxis)
    {
        float sourcePositionY = transform.position.y;
        transform.position=new Vector3(transform.position.x,sourcePositionY+sourceScale.magnitude/8,transform.position.z);

        while((sourceScale).magnitude>(transform.localScale).magnitude)
        {
            transform.localScale+=upAxis*player.sitSpeed;
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
        transform.localScale=sourceScale;
        yield break;
    }

    IEnumerator Jump()
    {
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.up*6,-Vector3.right*4));
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.up*6,-Vector3.right*4));
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.down*6,Vector3.right*4));
        yield return StartCoroutine(HalfTurn(Vector3.forward,Vector3.down*6,Vector3.right*4));
        yield return new WaitForSeconds(Time.fixedDeltaTime);
        yield return new WaitForSeconds(Time.fixedDeltaTime);
        yield return new WaitForSeconds(Time.fixedDeltaTime);
        yield return new WaitForSeconds(Time.fixedDeltaTime);
        StartCoroutine(TakeAnimation());
        yield break;
    }
}
