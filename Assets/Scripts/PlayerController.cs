using System.Collections;
using UnityEngine;
using BgBg.Plugins.Math.VectorExtensions;


public class PlayerController:MonoBehaviour
{
    enum playerNumber { Player1 = 1, Player2 = 2 }

    [HideInInspector]
    public bool leftAnimation=false;
    [HideInInspector]
    public bool rightAnimation = false;
    [HideInInspector]
    public bool jumpAnimation = false;
    [HideInInspector]
    public bool sitAnimation = false;

    [SerializeField]
    public float speed;
    [SerializeField]
    public float sitSpeed;
    [SerializeField]
    bool godMode;

    Vector3 sourceScale;
    [SerializeField]
    PlayerController anotherPlayer;
    [SerializeField]
    UIController uiController;
    [SerializeField]
    GameObject textToHide;
    [SerializeField]
    playerNumber _playerNumber;

    private void Start()
    {
        gameObject.AddComponent<RollingAnimations>();
    }
    private void LateUpdate()
    {
        switch(Input.GetAxis($"Horizontal{_playerNumber}"))
        {
            case -1:
                leftAnimation=true;
                break;
            case 1:
                rightAnimation=true;
                break;
            default:
                leftAnimation=false;
                rightAnimation=false;
                break;
        }
        switch(Input.GetAxis($"Vertical{_playerNumber}"))
        {
            case -1:
                jumpAnimation=true;
                break;
            case 1:
                sitAnimation=true;
                break;
            default:
                jumpAnimation=false;
                sitAnimation=false;
                break;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Finish")
        {
            uiController.showWinPanel();
            StopAllCoroutines();
            anotherPlayer.StopAllCoroutines();
        }
        if(godMode)
        {
            Debug.Log($"DEAD:{this.name}");
            return;
        }
        if(other.tag=="wall")
        {
            textToHide.SetActive(false);
            uiController.showWinPanel();
            StopAllCoroutines();
            anotherPlayer.StopAllCoroutines();
        }
    }
}