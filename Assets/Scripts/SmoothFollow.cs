using UnityEngine;

public class SmoothFollow: MonoBehaviour
{
    [SerializeField]
    Transform target;

    [SerializeField] 
    Vector3 offset;

    [SerializeField] 
    float smoothSpeed;

    private void FixedUpdate()
    {
        Vector3 distance = target.position-offset;
        transform.position=Vector3.Lerp(transform.position,distance,smoothSpeed);
    }

}
