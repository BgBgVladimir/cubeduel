using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FromPointToPointMower:MonoBehaviour
{
    [SerializeField]
    Vector3 first_point;
    [SerializeField]
    Vector3 second_point;
    [SerializeField]
    float moweSpeed;
    [SerializeField]
    bool direction;

    void FixedUpdate()
    {
        if(direction) transform.position=Vector3.MoveTowards(transform.position,first_point,moweSpeed);
        if(!direction) transform.position=Vector3.MoveTowards(transform.position,second_point,moweSpeed);
        if(transform.position==first_point) direction=false;
        if(transform.position==second_point) direction=true;
    }

    [ContextMenu("setMintPoint")]
    public void setMinPoint()
    {
        first_point=transform.position;
    }

    [ContextMenu("setMaxPosition")]
    void setMaxPoint()
    {
        second_point=transform.position;
    }

    [ContextMenu("randomizePosition")]
    void randomPositionBetweenTwoPoints()
    {
        transform.position=new Vector3(Random.Range(first_point.x,second_point.x),Random.Range(first_point.y,second_point.y),Random.Range(first_point.z,second_point.z));
    }
}
