using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FromPointToPointMower))]
public class FromPointToPointMowerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        FromPointToPointMower script = (FromPointToPointMower)target;
        if(GUILayout.Button("setMinPoint"))
        {
            script.setMinPoint();
        }
    }
}
